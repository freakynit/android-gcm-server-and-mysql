1. When you click GetRegId in app, the app registers with GCM and uploads registration id to my server at:
	http://ec2-54-184-223-163.us-west-2.compute.amazonaws.com

2. The latest registration id is available at:
	http://ec2-54-184-223-163.us-west-2.compute.amazonaws.com:4000/deviceTokens
		NOTE: The latest one is shown in top of the list
		----

3. Whole repo is divided into 3 projects:
	1. the client app ("client")
	2. the server to send notifications ("server"),
	3. the server where registration ids are received ("push-notification-testing-server")

	Note: "push-notification-testing-server" can be installed locally also:
		1. Run "sql_conf.sql" in "pushNotificationsTesting" project
		2. Make sure to change username and password in "config.json" file in "pushNotificationsTesting" project
		3. Then "npm install" as usual, and finally "node server.js"

3. Sample notification payload is present in file "data.js" in "pushNotificationsTesting" project, and "server.js" reads from it.

4. For client app, the starting java file is "MainActivity.java". In that:
	String PROJECT_NUMBER = "936312110931";
	specifies which project to use

5. Since client app uses gradle build system (alternative to maven, similar to npm for nodejs based projects), you need to install gradle build system on your pc. 

	IMPORTANT NOTE: "gradlew.bat" in already there in "client" project. Just run "gradlew installDebug" without double quotes. gradlew.bat automatically downloads required gradle build system and install it on your system and "installDebug" command compiles and installs app on device. The app doesn't start automaticcaly. The app is named: 
		"GcmClient"
	
	You can go through these:
		1. http://stackoverflow.com/questions/25063349/how-to-run-gradle-wrapper-on-windows-7
		2. https://docs.gradle.org/current/userguide/installation.html
