package com.nitinbansal85.java.android.gcmclient;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nitinbansal85.java.android.gcmclient.http.CachePolicy;
import com.nitinbansal85.java.android.gcmclient.http.NetworkObject;
import com.nitinbansal85.java.android.gcmclient.http.NetworkUtils;
import com.nitinbansal85.java.android.gcmclient.http.RequestMethod;
import com.nitinbansal85.java.android.gcmclient.http.Response;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by NitinBansal on 10/08/15.
 */
public class GcmMessageHandler extends IntentService {
    private Handler handler;

    private int hashedNotificationID = 0;

    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

//        String message = "title: " + extras.getString("title");
//        if(extras.containsKey("message")) {
//            message += ", message: " + extras.getString("message");
//        }
//        showToast(message);
        showToast("Data received");
        Log.i("GCM", "Received : (" + messageType + ")  " + extras.getString("title"));

        String notificationTemplate  = extras.getString("notificationTemplate");
        handlePushNotification(notificationTemplate);

        //showN(extras.getString("title"), extras.getString("message"));
        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }

    private Notification prepare(Context context, PushNotificationData pushNotificationData){
        List<Response> responses = new ArrayList<Response>();
        String URL = null;
        NetworkObject networkObject;
        if (pushNotificationData != null) {
            if (pushNotificationData.getLargeIcon() != null) {

                URL = pushNotificationData.getLargeIcon();
                networkObject = new NetworkObject.Builder(URL, RequestMethod.GET, getApplicationContext(), null)
                        .setCachePolicy(CachePolicy.GET_DATA_FROM_NETWORK_ONLY_NO_CACHING)
                        .setTag("largeIcon")
                        .build();
                responses.add(networkObject.execute());


            }

            if (pushNotificationData.getBigPictureStyleData() != null) {
                URL = pushNotificationData.getBigPictureStyleData().getBigPictureUrl();
                if (URL != null) {
                    networkObject = new NetworkObject.Builder(URL, RequestMethod.GET, getApplicationContext(), null)
                            .setCachePolicy(CachePolicy.GET_DATA_FROM_NETWORK_ONLY_NO_CACHING)
                            .setTag("bigPicture")
                            .build();
                    responses.add(networkObject.execute());
                }
            }


            Notification notification = build(context, pushNotificationData, responses);
            return notification;
        }
        return null;
    }

    private void handlePushNotification(String pushNotificationJson){
        try {
            JSONObject properties = new JSONObject(pushNotificationJson);
            PushNotificationData pushNotificationData = new PushNotificationData(getApplicationContext(), properties);

            Notification notificationData = prepare(this, pushNotificationData);

            if (pushNotificationData != null) {
                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(hashedNotificationID, notificationData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showN(String title, String message){
        NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(message) // message for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    public Notification build(Context context, PushNotificationData notificationData, List<Response> responses) {
        hashedNotificationID = notificationData.getNotificationID().hashCode();
        Map<String, Bitmap> images = new LinkedHashMap<String, Bitmap>();
        if (responses != null) {
            for (Response response : responses) {
                if (response.getTag().equals("largeIcon")) {
                    images.put(response.getTag(), NetworkUtils.decodeBitmap(context, response, 64f, 64f));
                } else if (response.getTag().equals("bigPicture")) {
                    images.put(response.getTag(), NetworkUtils.decodeBitmapByHeight(context, response, 192f));
                }

            }
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(notificationData.getNotificationTitle())
                .setContentText(notificationData.getNotificationContentText())
                .setSmallIcon(getApplicationContext().getApplicationInfo().icon);

        if (images.get("largeIcon") != null)
            mBuilder.setLargeIcon(images.get("largeIcon"));


        if (notificationData.isBigNotification() && notificationData.getStyle() != null) {
            switch (notificationData.getStyle()) {
                case BIG_TEXT:
                    if (notificationData.getBigTextStyleData() != null) {
                        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
                        bigTextStyle.setBigContentTitle(notificationData.getBigTextStyleData().getBigContentTitle());
                        bigTextStyle.bigText(notificationData.getBigTextStyleData().getBigText());
                        //bigTextStyle.setSummaryText(notificationData.getBigTextStyleData().getSummary());
                        mBuilder.setStyle(bigTextStyle);
                    }
                    break;
                case BIG_PICTURE:
                    if (notificationData.getBigPictureStyleData() != null) {
                        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
                        bigPictureStyle.setBigContentTitle(notificationData.getBigPictureStyleData().getBigContentTitle());
                        bigPictureStyle.setSummaryText(notificationData.getBigPictureStyleData().getSummary());
                        if (images.get("bigPicture") != null) {
                            bigPictureStyle.bigPicture(images.get("bigPicture"));
                            mBuilder.setStyle(bigPictureStyle);
                        }
                    }

                    break;

                case INBOX:
                    if (notificationData.getInboxStyleData() != null) {
                        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                        inboxStyle.setBigContentTitle(notificationData.getInboxStyleData().getBigContentTitle());
                        //inboxStyle.setSummaryText(notificationData.getInboxStyleData().getSummary());
                        List<String> lines = notificationData.getInboxStyleData().getInboxLines();
                        if (lines != null) {
                            for (String line : lines) {
                                inboxStyle.addLine(line);
                            }
                        }
                        mBuilder.setStyle(inboxStyle);
                    }
                    break;
            }
        }
        List<PushNotificationData.ActionButton> actionButtons = notificationData.getActionButtons();
        if (actionButtons != null && actionButtons.size() > 0) {
            for (PushNotificationData.ActionButton actionButton : actionButtons) {
                Intent ctaIntent = new Intent(AppEngageReceiver.APPENGAGE_ACTION);
                Bundle ctaBundle = new Bundle();
                ctaBundle.putString(AppEngageReceiver.ACTION, AppEngageReceiver.DEEPLINK_ACTION);
                ctaBundle.putString(DeepLinkActionController.URI, actionButton.getButtonActionUrl());
                ctaBundle.putString(AppEngageConstant.NOTIFICATION_ID, notificationData.getNotificationID());
                ctaBundle.putInt(AppEngageConstant.HASHED_NOTIFICATION_ID, hashedNotificationID);
                ctaBundle.putBoolean(AppEngageConstant.NOTIFICATION_MAIN_INTENT, false);
                ctaBundle.putString(AppEngageConstant.CTA_ID, actionButton.getButtonId());
                ctaIntent.putExtras(ctaBundle);
                mBuilder.addAction(0, actionButton.getButtonText(), PendingIntent.getBroadcast(getApplicationContext(), actionButton.getButtonId().hashCode(), ctaIntent, PendingIntent.FLAG_UPDATE_CURRENT));
            }
        }
        if (notificationData.getNotificationActionUrl() != null) {
            Intent mainIntent = new Intent(AppEngageReceiver.APPENGAGE_ACTION);
            Bundle bundle = new Bundle();
            bundle.putString(AppEngageReceiver.ACTION, AppEngageReceiver.DEEPLINK_ACTION);
            bundle.putString(DeepLinkActionController.URI, notificationData.getNotificationActionUrl());
            bundle.putString(AppEngageConstant.NOTIFICATION_ID, notificationData.getNotificationID());
            bundle.putInt(AppEngageConstant.HASHED_NOTIFICATION_ID, hashedNotificationID);
            bundle.putBoolean(AppEngageConstant.NOTIFICATION_MAIN_INTENT, true);
            mainIntent.putExtras(bundle);
            PendingIntent contentIntent = PendingIntent.getBroadcast(getApplicationContext(), notificationData.getNotificationID().hashCode(), mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(contentIntent);
        }
        Intent dIntent = new Intent(AppEngageReceiver.APPENGAGE_ACTION);
        String deleteIntentString = "appengage://" + getApplicationContext().getPackageName() + "/" + DeepLinkActionController.DEEPLINK_EVENTS.PUSH_NOTIFICATION_CANCELLED.getDeeplinkEventName() + "/" + notificationData.getNotificationID();
        dIntent.putExtra(AppEngageReceiver.ACTION, AppEngageReceiver.DEEPLINK_ACTION);
        dIntent.putExtra(DeepLinkActionController.URI, deleteIntentString);
        PendingIntent deleteIntent = PendingIntent.getBroadcast(getApplicationContext(), deleteIntentString.hashCode(), dIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setDeleteIntent(deleteIntent);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        if (notificationData.getVibrateFlag()) {
            if (ManifestUtils.checkPermission(context, ManifestUtils.MANIFEST_ELEMENTS.VIBRATE)) {
                notification.defaults |= Notification.DEFAULT_VIBRATE;
            }
        }
        if (notificationData.getSound() != null) {
            notification.sound = notificationData.getSound();
        }
        if (notificationData.getLedColor() != 0) {
            notification.flags |= Notification.FLAG_SHOW_LIGHTS;
            notification.ledARGB = notificationData.getLedColor();
            notification.ledOnMS = 500;
            notification.ledOffMS = 1000;
        }

        return notification;
    }

    public void showToast(final String message){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });

    }
}
