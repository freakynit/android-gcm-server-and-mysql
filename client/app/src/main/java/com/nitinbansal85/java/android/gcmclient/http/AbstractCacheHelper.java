package com.nitinbansal85.java.android.gcmclient.http;


import android.content.Context;

import com.nitinbansal85.java.android.gcmclient.AppEngageConstant;

import java.io.File;

abstract class AbstractCacheHelper {
   protected Context context;
   protected NetworkObject object;

   public AbstractCacheHelper(Context context, NetworkObject object) {
       this.context = context;
       this.object = object;
   }
   /**
    *  Establishes URL connection for the given network object
    * @return
    */
   public abstract Response downloadFile();

   public String getPathToDirectory() {
       File file = new File(context.getApplicationInfo().dataDir,
               AppEngageConstant.DIR_PATH);
       if (!file.exists()) {
           file.mkdirs();

       }
       return file.getPath();
   }

   public Response applyCachePolicy() {
       Response response;
       switch (object.getCachePolicy()) {
           case GET_DATA_FROM_NETWORK_ONLY_NO_CACHING:
               return downloadFile();
       }
       return null;
   }


}
