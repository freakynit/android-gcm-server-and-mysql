package com.nitinbansal85.java.android.gcmclient;

import android.content.Context;

/**
 * Created by NitinBansal on 13/08/15.
 */
public class ManifestUtils {
    public enum MANIFEST_ELEMENTS {
        INTERNET("android.permission.INTERNET"),
        READ_PHONE_STATE("android.permission.READ_PHONE_STATE"),
        ACCESS_WIFI_STATE("android.permission.ACCESS_WIFI_STATE"),
        BLUETOOTH("android.permission.BLUETOOTH"),
        ACCESS_FINE_LOCATION("android.permission.ACCESS_FINE_LOCATION"),
        ACCESS_NETWORK_STATE("android.permission.ACCESS_NETWORK_STATE"),
        WAKE_LOCK("android.permission.WAKE_LOCK"),
        GCM_RECEIVE("com.google.android.c2dm.permission.RECEIVE"),
        VIBRATE("android.permission.VIBRATE");

        private String manifest_elements;

        MANIFEST_ELEMENTS(String s) {
            this.manifest_elements = s;
        }

        public String getElement() {
            return this.manifest_elements;
        }
    }

    public static boolean checkPermission(Context context, MANIFEST_ELEMENTS permission) {
        int value = context.getPackageManager().checkPermission(permission.getElement(), context.getPackageName());
        return value == 0;
    }
}
