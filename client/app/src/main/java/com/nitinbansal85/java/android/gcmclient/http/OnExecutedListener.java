package com.nitinbansal85.java.android.gcmclient.http;

public interface OnExecutedListener {
     void onExecuted(Response response);
}
