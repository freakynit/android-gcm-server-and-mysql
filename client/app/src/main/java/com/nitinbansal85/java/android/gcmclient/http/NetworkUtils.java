package com.nitinbansal85.java.android.gcmclient.http;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class NetworkUtils {

    public static JSONArray getJSONArray(Response response) {
        JSONArray jsonArray = null;
        String data = readEntireStream(response);
        if (data == null) {
            return null;
        }
        try {
            jsonArray = new JSONArray(data);
        } catch (JSONException e) {
            return null;
        }
        return jsonArray;

    }

    public static JSONObject getJSONObject(Response response) {
        JSONObject jsonObject = null;
        String data = readEntireStream(response);
        if (data == null) {
            return null;
        }
        try {
            jsonObject = new JSONObject(data);
        } catch (JSONException e) {
            return null;
        }
        return jsonObject;

    }

    public static String readEntireStream(Response response) {
        if (response == null) {
            return null;
        }
        if (response.getException() != null) {
            return null;
        }
        InputStream inputStream = response.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line = "";
        try {
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            inputStream.close();
            String data = sb.toString();
            return data;
        } catch (Exception e) {
            return null;
        }

    }

    public static Bitmap decodeBitmap(Context context, Response response, float reqWidthInDP, float reqHeightInDP) {
        if (response == null) {
            return null;
        }
        if (response.getException() != null) {
            return null;
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int reqWidthInPixels = 0;
        int reqHeightInPixels = 0;
        if (reqWidthInDP != 0.0f) {
            reqWidthInPixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, reqWidthInDP, displayMetrics);
        } else {
            reqWidthInPixels = (displayMetrics.widthPixels);
        }
        if (reqHeightInDP != 0.0f) {
            reqHeightInPixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, reqHeightInDP, displayMetrics);
        } else {
            reqHeightInPixels = displayMetrics.heightPixels;
        }
        try {
            return loadBitmap(context, response, reqWidthInPixels, reqHeightInPixels);
        } catch (Exception e) {
            return null;
        }

    }

    public static Bitmap decodeBitmapByWidth(Context context, Response response, float reqWidthInDP) {
        return decodeBitmap(context, response, reqWidthInDP, 0.0f);
    }

    public static Bitmap decodeBitmapByHeight(Context context, Response response, float reqHeightInDP) {
        return decodeBitmap(context, response, 0.0f, reqHeightInDP);
    }


    private static Bitmap loadBitmap(Context context, Response response, int reqWidthInPixels, int reqHeightInPixles) throws Exception {

        File file = File.createTempFile(response.getCacheKey(), ".temp", context.getCacheDir());
        FileOutputStream fout = new FileOutputStream(file);
        InputStream inputStream = response.getInputStream();
        byte[] ar = new byte[1024];
        int length;
        while ((length = inputStream.read(ar)) != -1) {
            fout.write(ar, 0, length);
        }
        inputStream.close();
        fout.close();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        options.inSampleSize = calculateInSampleSize(options, reqWidthInPixels, reqHeightInPixles);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        double aspectRatioOfBitmap = (double) bitmapWidth / (double) bitmapHeight;
        double aspectRatioOfView = (double) reqWidthInPixels / (double) reqHeightInPixles;
        if (aspectRatioOfBitmap == aspectRatioOfView && bitmapWidth >= reqWidthInPixels && bitmapHeight >= reqHeightInPixles) {
            bitmap = Bitmap.createScaledBitmap(bitmap, reqWidthInPixels, reqHeightInPixles, false);
        }
        file.delete();
        return bitmap;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;


            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
