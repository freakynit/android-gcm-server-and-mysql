package com.nitinbansal85.java.android.gcmclient;

public class AppEngageReceiver {
    private static final String GCM_MESSAGE_ACTION = "com.google.android.c2dm.intent.RECEIVE";
    public static final String APPENGAGE_ACTION = "com.appengage.sdk.android.intent.ACTION";
    public static final String ACTION = "action";
    public static final String DEEPLINK_ACTION = "AppEngageDeeplink";
    public static final String LOCATION = "AppEngageLocation";
}
