package com.nitinbansal85.java.android.gcmclient;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

abstract class BaseNotificationDataHolder {
    protected Context context;

    private String title = null;
    private String message = null;
    private String mainCTA = null;
    private String mainCTAId = null;
    private String id = null;
    private boolean renderNotification = true;
    private Uri sound = null;
    private String type = null;
    private Map<String, Object> customData = null;
    private boolean vibrate = false;
    private int ledColor = 0;
    private JSONObject jsonObject = null;
    private String exp_id = null;

    public BaseNotificationDataHolder(){

    }

    public BaseNotificationDataHolder(JSONObject notificationJson, String type) throws JSONException {

        this.title = notificationJson.isNull("title") ? null : notificationJson.optString("title");
        if (this.title == null) {
            this.title = this.context.getResources().getString(R.string.push_notification_dummy_title);
        }
        if (notificationJson.isNull("message")) {
            throw new JSONException("message is Null");
        }
        this.message = notificationJson.optString("message");
        if (notificationJson.isNull("experimentId")) {
            throw new JSONException("experimentId is null");
        }
        this.exp_id = notificationJson.optString("experimentId");

        if (notificationJson.isNull("cta")) {
            throw new JSONException("Main CTA is NUll");
        }
        jsonObject = notificationJson.optJSONObject("cta");
        if (jsonObject.isNull("actionLink") || jsonObject.isNull("id")) {
            throw new JSONException("Main CTA Object Error");
        }
        this.mainCTA = jsonObject.optString("actionLink");
        this.mainCTAId = jsonObject.optString("id");
        this.renderNotification = true;
        this.sound = null;
        this.vibrate = false;
        this.ledColor = 0;

        if (notificationJson.isNull("identifier")) {
            throw new JSONException("Notification ID is Null");
        }
        this.id = notificationJson.optString("identifier");
        this.type = type;
        JSONArray customDataArray = notificationJson.isNull("custom") ? null : notificationJson.optJSONArray("custom");
        if (customDataArray != null) {
            customData = new HashMap<String, Object>();
            for (int i = 0; i < customDataArray.length(); i++) {
                try {
                    JSONObject jsonObject = customDataArray.getJSONObject(i);
                    customData.put(jsonObject.getString("key"), jsonObject.get("value"));
                } catch (JSONException e) {

                }

            }
        }

    }

    public String getNotificationType() {
        return this.type;
    }

    public String getNotificationID() {
        return this.id;
    }

    public void setVisibility(boolean renderNotification) {
        this.renderNotification = renderNotification;
    }

    public void setVibrateFlag(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public boolean getVibrateFlag() {
        return this.vibrate;
    }

    public void setLedLight(int argb) {
        this.ledColor = argb;
    }

    public int getLedColor() {
        return this.ledColor;
    }


    public void setSound(Uri sound) {
        this.sound = sound;
    }


    public String getNotificationTitle() {
        return this.title;
    }

    public void setNotificationTitle(String title) {
        this.title = title;
    }

    public String getNotificationContentText() {
        return this.message;
    }

    public void setNotificationContentText(String text) {
        this.message = text;
    }

    public String getNotificationActionUrl() {
        return this.mainCTA;
    }

    public String getNotificationCTAId() {
        return this.mainCTAId;
    }

    public void setNotificationActionUrl(String url) {
        this.mainCTA = url;
    }

    public boolean getVisibility() {
        return this.renderNotification;
    }

    public Uri getSound() {
        return this.sound;
    }

    public Map<String, Object> getCustomData() {
        return this.customData;
    }

    public void setCustomData(Map<String, Object> customData) {
        this.customData = customData;
    }

    public String getExperimentId() {
        return this.exp_id;
    }

}

