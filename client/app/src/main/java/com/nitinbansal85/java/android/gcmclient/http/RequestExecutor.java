package com.nitinbansal85.java.android.gcmclient.http;


import android.content.Context;

import com.nitinbansal85.java.android.gcmclient.AppEngageConstant;
import com.nitinbansal85.java.android.gcmclient.AppEngageUtils;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.net.ssl.HttpsURLConnection;

class RequestExecutor extends AbstractCacheHelper {
    String encodedURL = null;
    Map<String, Object> result = null;

    public RequestExecutor(Context context, NetworkObject object) {
        super(context, object);
        this.encodedURL = hashUrl(object.getURL());
    }

    private String hashUrl(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return url;
        }
    }

    @Override
    public Response downloadFile() {
        return establishConnection();
    }


    private Response establishConnection() {
        HttpURLConnection con = null;
        OutputStream outputStream = null;
        Response response = new Response();
        try {
            con = (HttpURLConnection) new URL(super.object.getURL()).openConnection();
            con.setRequestMethod(super.object.getRequestMethod().toString());
            con.setConnectTimeout(AppEngageConstant.DEFAULT_CONNECT_TIMEOUT);
            con.setReadTimeout(AppEngageConstant.DEFAULT_READOUT_TIME);
            if (super.object.getHeaders() != null) {
                Map<String, String> headers = super.object.getHeaders();
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            if (!RequestMethod.GET.toString().equalsIgnoreCase(super.object.getRequestMethod().toString())) {
                con.setDoOutput(true);
            }
            con.setDoInput(true);

            Object params = super.object.getParams();
            if (params != null) {
                if (object.getCompressBodyState()) {
                    con.setRequestProperty("Content-Encoding", "gzip");
                    outputStream = con.getOutputStream();
                    GZIPOutputStream gzipOutputStream = new GZIPOutputStream(outputStream);
                    Writer writer = new OutputStreamWriter(gzipOutputStream);
                    writeToOutputStream(writer, params);
                    gzipOutputStream.close();
                    outputStream.close();
                } else {
                    outputStream = con.getOutputStream();
                    Writer writer = new OutputStreamWriter(outputStream);
                    writeToOutputStream(writer, params);
                    outputStream.close();
                }

            }
            response.setResponseCode(con.getResponseCode());
            if (con.getResponseCode() == HttpsURLConnection.HTTP_NOT_MODIFIED && RequestMethod.GET.equals(super.object.getRequestMethod())) {
                response.setModifiedState(false);
            }
            response.setHeaders(con.getHeaderFields());
            if (response.getResponseCode() != HttpsURLConnection.HTTP_NOT_MODIFIED) {

                if (AppEngageUtils.CheckGZIP(con)) {
                    response.setInputStream(new GZIPInputStream(con.getInputStream()));

                } else {
                    response.setInputStream(con.getInputStream());
                }
            }

            return response;

        } catch (Exception e) {
            response.setException(e);
            return response;
        }
    }

    private void writeToOutputStream(Writer writer, Object params) throws Exception {
        if (params instanceof Map<?, ?>) {
            writer.write(AppEngageUtils.getParams((Map<String, String>) params));
        } else {
            writer.write(params.toString());
        }
        writer.close();

    }


}
