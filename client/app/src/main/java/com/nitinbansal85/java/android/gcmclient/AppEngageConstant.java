package com.nitinbansal85.java.android.gcmclient;

public class AppEngageConstant {
    public static final String NOTIFICATION_ID = "id";
    public static final String HASHED_NOTIFICATION_ID = "hashed_notification_id";
    public static final String NOTIFICATION_MAIN_INTENT = "notification_main_intent";
    public static final String CTA_ID = "call_to_action";
    public static final String EXPERIMENT_ID = "experiment_id";
    public static final String NOTIFICATION_TYPE = "type";

    public static final String DIR_PATH = ".AppEngageCahce";

    public static final int DEFAULT_READOUT_TIME = 20000;
    public static final int DEFAULT_CONNECT_TIMEOUT = 20000;

    public enum STYLE {
        BIG_PICTURE,
        BIG_TEXT,
        INBOX

    }

}

