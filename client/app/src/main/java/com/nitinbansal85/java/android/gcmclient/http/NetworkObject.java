package com.nitinbansal85.java.android.gcmclient.http;

import android.content.Context;

import java.util.Map;

public class NetworkObject {
    private final String url;
    private final RequestMethod method;
    private Map<String, String> headers;
    private final Object params;
    private final String tag;
    private final CachePolicy cachePolicy;
    private Context context;
    private boolean shouldComrpessBody;
    private OnExecutedListener listener;

    private NetworkObject(Context context, Builder builder) {
        this.url = builder.url;
        this.method = builder.method;
        this.headers = builder.headers;
        this.params = builder.params;
        this.tag = builder.tag;
        this.cachePolicy = builder.cachePolicy;
        this.context = builder.context;
        this.shouldComrpessBody = builder.shouldCompressBody;
        this.listener = builder.listener;

    }

    public static class Builder {
        private final String url;
        private final RequestMethod method;
        private Map<String, String> headers = null;
        private Object params = null;
        private String tag = null;
        private Context context = null;
        private CachePolicy cachePolicy = CachePolicy.GET_DATA_FROM_NETWORK_ONLY_NO_CACHING;
        private boolean shouldCompressBody = false;
        private OnExecutedListener listener = null;

        public Builder(String url, RequestMethod method, Context context, OnExecutedListener listener) {
            this.url = url;
            this.method = method;
            this.context = context;
            this.listener = listener;
        }

        public Builder setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        public Builder setParams(Object params) {
            this.params = params;
            return this;
        }

        public Builder setTag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder setCachePolicy(CachePolicy policy) {
            this.cachePolicy = policy;
            return this;
        }

        public void shouldCompressBody(boolean state) {
            this.shouldCompressBody = state;
        }

        public NetworkObject build() {
            return new NetworkObject(this.context, this);
        }
    }

    public String getURL() {
        return this.url;
    }

    public RequestMethod getRequestMethod() {
        return this.method;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public Object getParams() {
        return this.params;
    }

    public String getTag() {
        return this.tag;
    }

    public CachePolicy getCachePolicy() {
        return this.cachePolicy;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;

    }

    public boolean getCompressBodyState() {
        return this.shouldComrpessBody || (this.headers != null && (this.headers.get("Content-Encoding") != null && this.headers.get("Content-Encoding").equals("gzip")));
    }

    public Response execute() {
        RequestExecutor requestExecutor = new RequestExecutor(this.context, this);
        Response response = requestExecutor.applyCachePolicy();
        response.setTag(this.tag);
        response.setModifiedState(response.modified() && !this.cachePolicy.equals(CachePolicy.GET_DATA_FROM_NETWORK_ONLY_NO_CACHING));
        response.setCacheKey(requestExecutor.encodedURL);
        if (listener != null) {
            listener.onExecuted(response);
        }
        return response;


    }


}


