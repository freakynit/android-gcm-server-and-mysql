package com.nitinbansal85.java.android.gcmclient;

import java.net.HttpURLConnection;
import java.util.Map;

public class AppEngageUtils {
    public static boolean CheckGZIP(HttpURLConnection con) {
        String encoding = con.getContentEncoding();
        return encoding != null
                && (encoding.equals("gzip") || encoding.equals("zip") || encoding
                .equals("application/x-gzip-compressed"));

    }

    public static String getParams(Map<String, String> params) {
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(sb.length() == 0 ? "" : "&");
            sb.append(entry.getKey()).append("=").append(entry.getValue());
        }

        return sb.toString();
    }
}
