var PORT = 4000;

var MYSQL_HOST = "localhost";
var MYSQL_PORT = 3306;
var MYSQL_USER = "root";
var MYSQL_PASSWORD = "admin123A@";
var MYSQL_DB = "pushNotificationsTesting";

var nconf = require('nconf');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mime = require('mime');
var fs = require('fs');
var mysql      = require('mysql');


nconf.argv()
       .env()
       .file({file: 'config.json'});

nconf.defaults({
    'server': {
        'port': PORT
    },
    'mysql': {
        'host': MYSQL_HOST,
        'port': MYSQL_PORT,
        'user': MYSQL_USER,
        'password': MYSQL_PASSWORD,
        'db': MYSQL_DB
    }
});



var app = express();

var mysqlConnection = mysql.createConnection({
  host     : nconf.get('mysql:host'),
  port     : nconf.get('mysql:port'),
  user     : nconf.get('mysql:user'),
  password : nconf.get('mysql:password'),
  database : nconf.get('mysql:db')
});


mysqlConnection.connect(function(err) {
  if(err) {
    console.log("Error in connecting to mysql: ", err);
  } else {
    console.log("Connected correctly to mysql server");
    
    app.all('*', function(req, res, next) {
        req.mysqlConn = mysqlConnection;
        next();
    });

    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(session({
        secret : "SHUUUUSH",
        saveUninitialized: true,
        resave : false 
    }));

    var updateDeviceToken = require('./routes/updateDeviceToken');
    var index = require('./routes/index');

    app.use(updateDeviceToken);
    app.use(index);

    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.json({
                "message": err.message,
                "error": err
            });
        });
    }

    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            "message": err.message,
            "error": {}
        });
    });

    app.listen(nconf.get('server:port'), function(){
      console.log("Started on PORT " + nconf.get('server:port'));
    });

  }
});

