//https://android.googleapis.com/gcm/send
//Authorization: key=AIzaSyDzuJxiGqFU23usgkMr7abR4WU6Fe43jTw
//Content-Type: application/json

var gcm = require('node-gcm');
var regIds = require('./reg_ids.json')
var data = require('./data.json')

var sender = new gcm.Sender('AIzaSyDzuJxiGqFU23usgkMr7abR4WU6Fe43jTw');

var message = new gcm.Message({
	"collapseKey": 'demo',
    "priority": 3,
    "contentAvailable": true,
    "timeToLive": 3,
    "delayWhileIdle": false,
    "data": data
    //data: {"notificationTemplate":{"identifier":"some_id_1","title":"New Offers on Shirts","message":"Get upto 50% discount","experimentId":"kjb","image":"https://cdn2.iconfinder.com/data/icons/despicable-me-2-minions/128/despicable-me-2-Minion-icon-5.png","cta":{"id":"main_cta_1","type":"EXTERNAL_URL","actionLink":"appengage://testapp.com/open_url_in_browser/http%3A%2F%2Fwww.webengage.com"},"custom":[{"key":"name","value":"shahrukh"},{"key":"age","value":23}],"expandableDetails":{"style":"BIG_TEXT","title":"Purchase new Polo T-Shirts","message":"At Flipkart, we believe that a wish is a desire that should be fulfilled for every Indian. And that is why you’ll see that there’s always something for you to shop for. Whether it’s an IIT-JEE entrance book or the latest Mi phone, you’ll find it here on Flipkart. Home to a huge collection of home furnishing products, appliances, footwear, television, clothes, mobiles, books, cameras and laptops, the list is as endless as the potential long line that you would end up in should you happen to brave the crowd at the end of your shopping trip at a mall","cta1":{"id":"id1","type":"EXTERNAL_URL","actionLink":"appengage://testapp.com","actionText":"BTN 1"},"cta2":{"id":"id2","type":"EXTERNAL_URL","actionLink":"appengage://testapp.com","actionText":"BTN 2"},"cta3":{"id":"id3","type":"EXTERNAL_URL","actionLink":"appengage://testapp.com","actionText":"BTN 3"}}}}
});

sender.send(message, regIds, function (err, result) {
    if(err) console.error(err);
    else    console.log(result);
});
